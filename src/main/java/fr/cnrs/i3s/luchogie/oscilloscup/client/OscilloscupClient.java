package fr.cnrs.i3s.luchogie.oscilloscup.client;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class OscilloscupClient {
    private final Socket socket;
    private final OutputStream os;
    public OscilloscupClient(String ip, int port) throws IOException {
        this.socket = new Socket(ip, port);
        this.os = socket.getOutputStream();
    }
    public void addPoint(String plotName, int figureIndex, double x, double y)
            throws IOException {
        String s = plotName + "/" + figureIndex + "/" + x + "/" + y;
        os.write(s.getBytes());
        os.write('\n');
    }

    public void addPoint(String plotName, int figureIndex, double x, double y, String pointColor)
            throws IOException {
        String s = plotName + "/" + figureIndex + "/" + x + "/" + y+"/"+pointColor;
        os.write(s.getBytes());
        os.write('\n');
    }
}