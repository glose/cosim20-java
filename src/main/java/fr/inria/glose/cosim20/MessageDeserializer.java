package fr.inria.glose.cosim20;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import java.io.IOException;
import java.math.BigDecimal;

public class MessageDeserializer extends StdDeserializer<Message> {

    public MessageDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Message deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String id = node.get("id").asText();
        String source = node.get("source").asText();
        double temporalHorizonString = node.get("temporalHorizon").asDouble();
        BigDecimal temporalHorizon = Utils.toBigDecimal(temporalHorizonString);
        // Retrieve the Type of the action to execute
        Action.TypeOfAction typeOfAction = Action.TypeOfAction.valueOf(node.get("typeOfAction").asText("UNKNOWN"));

        JsonNodeType valueString = node.get("value").getNodeType();
        if (valueString == JsonNodeType.MISSING) {
            return new Message(typeOfAction, id, source, temporalHorizon);
        }

        if (valueString == JsonNodeType.NUMBER) {
            return new Message(typeOfAction, id, source, temporalHorizon, node.get("value").doubleValue());
        } else if (valueString == JsonNodeType.BOOLEAN) {
            return new Message(typeOfAction, id, source, temporalHorizon, node.get("value").booleanValue());
        }
        return new Message(typeOfAction, id, source, temporalHorizon, node.get("value").asText());
    }
}
