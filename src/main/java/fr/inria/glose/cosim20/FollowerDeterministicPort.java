package fr.inria.glose.cosim20;

import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;

import java.math.BigDecimal;

public class FollowerDeterministicPort extends Port {

    public TemporalPredicate temporalPredicate;
    public BigDecimal timeOfLastSample = new BigDecimal(0.0);

    public FollowerDeterministicPort(String ID, String modelVarName, String modelID,  Object value, TemporalPredicate deterministicTemporalPredicate) {
        super(ID, modelVarName, modelID, value);
        this.temporalPredicate = deterministicTemporalPredicate;
    }

    public double getStepSize() {
        return temporalPredicate.deltaT;
    }
}
