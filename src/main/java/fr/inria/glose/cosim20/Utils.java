package fr.inria.glose.cosim20;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

public class Utils {

    public static final int SCALE = 4;

    private static final double  EPSILON = 0.0001;
    /**
     * Compare two {@code double} values
     * @param a some <i>double</i> value
     * @param b some other <i>double</i> value
     * @return {@code true} if the two values are equal
     */
    public static boolean equals (final double a, final double b) {
        if (a==b) return true;
        return Math.abs(a - b) < EPSILON; //EPSILON = 0.0000001d
    }

    /**
     * Compare {@code a} and {@code b}
     * @param a some <i>double</i> value
     * @param b some other <i>double</i> value
     * @return a negative value if {@code a} is smaller than {@code b},
     * a positive value if {@code a} is larger than {@code b}, else {code 0}.
     */
    public static int compare (final double a, final double b) {
        return equals(a, b) ? 0 : (a < b) ? -1 : +1;
    }

    public static boolean isMod(final double a, final double b) {
        BigDecimal x = new BigDecimal(a, MathContext.DECIMAL64);
        return x.remainder(new BigDecimal(b, MathContext.DECIMAL64)).compareTo(BigDecimal.ZERO) == 0;
    }

    public static boolean isMod(final BigDecimal a, final BigDecimal b) {
        return a.remainder(b).compareTo(BigDecimal.ZERO) == 0;
    }

    public static BigDecimal toBigDecimal(double timestamp) {
        return BigDecimal.valueOf(timestamp).setScale(SCALE, RoundingMode.HALF_UP);
    }


    public static BigDecimal getNextTH(FollowerDeterministicPort dp, BigDecimal now){
        BigDecimal deltaT = Utils.toBigDecimal(dp.getStepSize());
        //deltaT - (now % DeltaT)
        BigDecimal usualNextTH = deltaT.subtract(now.remainder(deltaT));
        if (usualNextTH.compareTo(deltaT) == 0){ //here, either we are waiting for the 'fresh' sample or not
            if(dp.timeOfLastSample.compareTo(now) < 0) { //we are waiting for it
                return new BigDecimal(0.0);
            }
        }
        return usualNextTH;
    }


    public static BigDecimal getDeterministicMinNextTH(List<FollowerDeterministicPort> followerPorts, BigDecimal now) {
        Optional<BigDecimal> minNextTH = followerPorts.stream().map(dPort -> getNextTH(dPort, now)).min(BigDecimal::compareTo);

        if (minNextTH.isPresent()) {
            return minNextTH.get();
        }
        return Utils.toBigDecimal(0.0);
    }
}
