package fr.inria.glose.cosim20;

import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;

import java.math.BigDecimal;

public class Action implements Comparable<Action> {

    public Port port;
    public Object value;
    public BigDecimal temporalHorizon;
    protected boolean done;
    public String hostSource;
    private CoordinationPredicate predicate;
    public enum TypeOfAction{
        UPDATETH("UPDATETH"), SET("SET"), RETRIEVE("RETRIEVE"), FAKE("FAKE"), PUBLISH("PUBLISH"), TERMINATE("TERMINATE");

        private String actioname;
        TypeOfAction(String name) {
            this.actioname = name;
        }
        @Override
        public String toString(){
            return actioname;
        }
    }
    public TypeOfAction typeOfAction;

    public Action(TypeOfAction typeOfAction, BigDecimal temporalHorizon) {
        this.temporalHorizon = temporalHorizon;
        this.typeOfAction = typeOfAction;
    }

    @Override
    public int compareTo(Action action) {
        return temporalHorizon.compareTo(action.temporalHorizon);
    }

    public boolean isDone() {
        return done;
    }

    public void setDone() {
        this.done = true;
        if(this.port instanceof FollowerDeterministicPort){
            ((FollowerDeterministicPort)port).timeOfLastSample = this.temporalHorizon;
        }
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public CoordinationPredicate getPredicate() {
        return predicate;
    }

    public void setPredicate(CoordinationPredicate predicate) {
        this.predicate = predicate;
    }
}
