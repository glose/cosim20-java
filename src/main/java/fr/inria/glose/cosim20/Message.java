package fr.inria.glose.cosim20;
import java.io.Serializable;
import java.math.BigDecimal;

public class Message implements Serializable,Comparable<Message> {

    // Identifier
    public String id;

    // Timestamp
    public BigDecimal temporalHorizon;

    // value
    public Object value;

    // Source
    public String source;

    /*
     * Quick fix to know if the message contains the value or not. If not, it means the message
     * is an UpdateTH action
     */
    public Action.TypeOfAction typeOfAction;

    public Message(Action.TypeOfAction typeOfAction, String id, String source, BigDecimal temporalHorizon, Object value) {
        this.id = id;
        this.temporalHorizon = temporalHorizon;
        this.value = value;
        this.source = source;
        this.typeOfAction = typeOfAction;
    }

    public Message(Action.TypeOfAction typeOfAction, String id, String source, BigDecimal temporalHorizon) {
        this.id = id;
        this.temporalHorizon = temporalHorizon;
        this.source = source;
        this.typeOfAction = typeOfAction;
    }

    public Message(Action.TypeOfAction typeOfAction, String id, String source, BigDecimal temporalHorizon, String value) {
        this.id = id;
        this.temporalHorizon = temporalHorizon;
        this.source = source;
        this.typeOfAction = typeOfAction;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int compareTo(Message message) {
        return this.temporalHorizon.compareTo(message.temporalHorizon);
    }

}
