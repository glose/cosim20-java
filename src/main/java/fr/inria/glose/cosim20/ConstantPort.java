package fr.inria.glose.cosim20;

public class ConstantPort extends Port {
    public ConstantPort(String ID, String modelVarName, String modelID, Object value) {
        super(ID, modelVarName, modelID, value);
    }
}
