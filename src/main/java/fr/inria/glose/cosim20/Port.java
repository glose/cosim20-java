package fr.inria.glose.cosim20;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Port implements Serializable, Comparable<Port> {

    public String ID; // ID to identify the port
    public String associatedModelVariableName; // Associated internal variable name
    public Object value; // Actual value of the port
    public String modelID;
    public String topic;
    public PortFunction executeFunction;

    public Port(String ID, String modelVarName, String modelID, Object value) {
        this.ID = ID;
        this.value = value;
        this.associatedModelVariableName = modelVarName;
        this.topic = ID;
        this.modelID = modelID;
    }

    @Override
    public String toString() {
        return "Port{" +
                "ID='" + ID + '\'' +
//                ", associatedModelVariableName='" + associatedModelVariableName + '\'' +
//                ", value=" + value +
//                ", modelID='" + modelID + '\'' +
//                ", topic='" + topic + '\'' +
//                ", executeFunction=" + executeFunction +
//                ", linksMap=" + linksMap +
                '}';
    }

    public String getModelID() {
        return modelID;
    }

    public int compareTo(Port o) {
        return o.ID.compareTo(this.ID);
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public void addExecuteMethod(PortFunction executeFunction){
        this.executeFunction = executeFunction;
    }

    // ---------------------------------------------------------------------------------------------
    // Communication related part for Output port (each connected input port will have the
    // corresponding port to connect to.
    // ---------------------------------------------------------------------------------------------

    // Map externalPortName -> Socket
    protected Map<String, ZMQ.Socket> linksMap = new HashMap<>();

    public void connectToInputPort(ZContext context, String externalPortName, int port, int syncPort) {
        ZMQ.Socket publisher = context.createSocket(SocketType.PUB);
        // publisher
        if (publisher.bind("tcp://*:"+port)) {
            System.out.println("Publisher created on tcp://*:"+port);
        } else {
            System.out.println("Error: Publisher not created");
        }
        linksMap.put(externalPortName, publisher);
    }

    /**
     * Propagate sends to each linked port the updated value
     */
    public void propagate() {

    }
}

