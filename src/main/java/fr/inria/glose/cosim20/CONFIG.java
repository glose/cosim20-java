package fr.inria.glose.cosim20;

import java.math.BigDecimal;

public class CONFIG {
    public static boolean showDebugMessage = false;
    public static BigDecimal InfOfSimulation = new BigDecimal(Integer.MAX_VALUE);
    public static BigDecimal EndOfSimulation = Utils.toBigDecimal(50000.0);
    public static BigDecimal StartOfSimulation = Utils.toBigDecimal(0.0);
}
