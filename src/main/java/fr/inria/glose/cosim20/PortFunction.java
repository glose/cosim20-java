package fr.inria.glose.cosim20;

public interface PortFunction {
    void execute();
}
