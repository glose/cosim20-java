package fr.inria.glose.cosim20;

import java.math.BigDecimal;

public class ActionWithPredicate extends Action {
    public ActionWithPredicate(BigDecimal temporalHorizon) {
        super(TypeOfAction.FAKE, temporalHorizon);
    }
}
