package fr.inria.glose.cosim20;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import fr.inria.glose.cosim20.interfaces.Interface;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.COSFilterInputStream;
import org.eclipse.gemoc.execution.commons.commands.*;
import org.eclipse.gemoc.execution.commons.predicates.BinaryPredicate;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;
import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQException;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public abstract class CoordinationInterface implements Runnable {

    enum BIState {
        INITIALIZING,
        RUNNING,
        WAIT_Set,
        WAIT_TH,
        TERMINATING
    }

    // ----------------------------------------------------------------
    // Simulation
    // ----------------------------------------------------------------
    public Interface model;
    protected String ID;
    protected CoordinationPredicate initiatorsPredicate;
    public BIState state = BIState.INITIALIZING;

    // ----------------------------------------------------------------
    // ZeroMQ
    // ----------------------------------------------------------------
    protected ZMQ.Socket publisher;
    String hostname;
    int port;
    ZContext context;
    int syncPort;
    int connectedQueues;

    // ----------------------------------------------------------------
    // Synchronization
    // ----------------------------------------------------------------
    public Semaphore waitTH = new Semaphore(0);
    public Semaphore waitR2Rport = new Semaphore(0);
    private final Integer lock4now = 1;

//    private final Integer myMonitorObject = 0;
//    boolean wasSignalled = false;
//    private final Object testObjectSync = new Object();
//    private final Object testFollowerSync = new Object();

    // ----------------------------------------------------------------
    // Temporal Horizon handler
    // ----------------------------------------------------------------
    protected int numberOfInitiatorsToWaitfor;
    protected List<Callable<Object>> calls = new ArrayList<>();
    protected List<Port> allPorts = new ArrayList<>();
    /**
     * ports for which the simulation unit is initiating the transaction
     */
    protected List<Port> initiatorPorts = new ArrayList<>();
    /**
     * ports for which the simulation unit is waiting for a transaction
     */
    protected List<Port> followerPorts = new ArrayList<>();
    /**
     * ports for which the simulation unit is waiting for a transaction but can predict when it will occur
     */
    protected List<FollowerDeterministicPort> followerDeterministicPorts = new ArrayList<>();
    protected PriorityBlockingQueue<Action> todo = new PriorityBlockingQueue<>();
    protected ConcurrentHashMap<String, BigDecimal> initiatorsTemporalHorizons = new ConcurrentHashMap<>();
    // -----------------------------------------------------------------------------------------------------------------
    // Compile time generated maps
    // -----------------------------------------------------------------------------------------------------------------
    protected Map<String, Port> portMap = new HashMap<>();

    // ----------------------------------------------------------------
    // Algorithm specification data
    // ----------------------------------------------------------------
    protected BigDecimal now = Utils.toBigDecimal(0.0);
    protected BigDecimal actualStepSize = Utils.toBigDecimal(0.0);
    protected BigDecimal nextDeterministicTemporalHorizon = Utils.toBigDecimal(0.0);
    protected BigDecimal nextTimeToStop = Utils.toBigDecimal(0.0);
    protected BigDecimal nextInitiatorsTemporalHorizon = Utils.toBigDecimal(0.0);


    // ----------------------------------------------------------------
    // Methods to implement
    // ----------------------------------------------------------------
    public abstract void onTime(Action currentAction, StopCondition sr);

    public abstract void onEvent(Action currentAction, StopCondition sr);

    public abstract void onReadyToRead(Action currentAction, StopCondition sr);

    public abstract void onUpdated(Action currentAction, StopCondition sr);

    public abstract void onSync(Action currentAction);

    public abstract void onEnd();

    public abstract CoordinationPredicate setInitiatorsPredicate();

    /**
     * CoordinationInterface instantiates
     * @param ID Unique Identifier used to identify the simulation unit in the global system
     * @param hostname IP or FQDN of the current host of the simulation unit
     * @param port Port on which the ZeroMQ is listening
     * @param syncPort Port on which the connected models must synchronize BEFORE trying to connect to the ZeroMQ queue defined on port
     * @param connectedInitiators Number of connected initiators port which are not deterministic (not in followerPortDeterminist). Or it is the number of Follower ports
     * @param connectedQueues      Number of connected queues
     */
    public CoordinationInterface(String ID, String hostname, int port, int syncPort, int connectedInitiators, int connectedQueues) {
        this.port = port;
        this.syncPort = syncPort;
        this.hostname = hostname;
        this.numberOfInitiatorsToWaitfor = connectedInitiators;
        this.connectedQueues = connectedQueues;
        this.ID = ID;

        context = new ZContext();
        publisher = context.createSocket(SocketType.PUB);
        // publisher
        if (publisher.bind("tcp://*:" + port)) {
            System.out.println("[" + ID + "@" + now + "] Publisher created on tcp://*:" + port);
        } else {
            System.err.println("[" + ID + "@" + now + "] Publisher not created");
        }
    }

    /**
     * Add a new input port on the model by specifying the target properties
     * @param externalPortID ID of the target port on the target simulation unit
     * @param externalHostname URL composed by the host:port of the target simulation unit
     * @param syncExternalHostname URL composed by the host:port of the target simulation unit to SYNC before the actual connection
     */
    protected void addNewInputPort(String externalPortID, String externalHostname, String syncExternalHostname) {
        calls.add(Executors.callable(new SubscriptionService(externalHostname, externalPortID, syncExternalHostname)));
    }

    @Override
    public void run() {
        calls.add(Executors.callable(this::internalRunModel));
        ExecutorService executor = Executors.newFixedThreadPool(calls.size());
        // Start the model
        try {
            executor.invokeAll(calls);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to actually run the simulation unit connected to the Model Coordination Interface
     */
    private void internalRunModel() {
        try {
            sync("tcp://*:" + syncPort, connectedQueues);
            algorithm();
        } catch (Exception e) {
            System.err.println("[" + ID + "@" + now + "] problem in InternalRunModel: " + e);
            e.printStackTrace();
        }
    }

    /**
     * Blocking method to synchronize the beginning of the co-simulation with the connected Coordination Interfaces
     * @param syncHostname URL of the current Coordination Interface instance
     * @param listeningSubscribers Number of the subscribers that must be connected with the specified instance
     */
    private void sync(String syncHostname, int listeningSubscribers) {
        //  Socket to receive signals
        ZMQ.Socket syncservice = context.createSocket(SocketType.REP);
        syncservice.bind(syncHostname);

        //  Get synchronization from subscribers
        int subscribers = 0;

        while (subscribers < listeningSubscribers) {
            if (CONFIG.showDebugMessage)
                System.out.println("[" + ID + "@" + now + "] Waiting for " + (listeningSubscribers - subscribers) + " subscribers on " + syncHostname);
            //  - wait for synchronization request
            syncservice.recv(0);

            //  - send synchronization reply
            syncservice.send("", 0);
            subscribers++;
        }
        if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now + "] Done");
    }

    /**
     * Blocking method that waits until the conditions on the temporal horizon barrier are met. If all the initiators temporal horizon are known then simulation can proceed
     */
    public void waitTH() {
        // Check if I have to wait or not. If all the initiators temporal horizon are known then simulation can proceed
        if (numberOfInitiatorsToWaitfor == 0) {
            return;
        }
        try {
            state = BIState.WAIT_TH;
            waitTH.acquire();
            state = BIState.RUNNING;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Blocking method that wait on a ReadyToRead condition until the other Simulation Unit returns the correct value at the correct timestamp
     */
    public void waitR2Rport() {
        //  wait answer from the other SU
        try {

            waitR2Rport.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * TODO
     */
    public void notifyTH() {
        if (initiatorsTemporalHorizons.size() == numberOfInitiatorsToWaitfor) {
            synchronized (this) {
                int nbP = waitTH.availablePermits();
                if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now + "] availablePermits =" + nbP);
                if (nbP == 0) {
                    waitTH.release();//no cumul
                }
            }
        }
    }

    /**
     * TODO
     */
    public void notifyR2R() {
        waitR2Rport.release();
    }

    /**
     * Send the current Temporal Horizon to the specified port
     * @param port Port to send the current TH
     * @param temporalHorizon Current TH to send
     */
    protected void updateTH(Port port, BigDecimal temporalHorizon) {
        Message m = new Message(Action.TypeOfAction.UPDATETH, port.ID, "tcp://" + hostname + ":" + this.port, temporalHorizon);
        sendMessage(publisher, m, port.ID);
    }

    /**
     * Publish the specified value on the port with the defined timestamp (Temporal Horizon)
     * @param port Port to send the value and the timestamp
     * @param value Value to send
     * @param temporalHorizon Temporal Horizon or timestamp to send
     */
    protected void publish(Port port, Object value, BigDecimal temporalHorizon) {
        Message m = new Message(Action.TypeOfAction.SET, port.ID, "tcp://" + hostname + ":" + this.port, temporalHorizon, value);
        sendMessage(publisher, m, port.ID);
    }

    /**
     * TODO
     * @param port
     * @param temporalHorizon
     */
    protected void retrieve(Port port, BigDecimal temporalHorizon) {
        Message m = new Message(Action.TypeOfAction.RETRIEVE, port.ID, this.ID, temporalHorizon);
        sendMessage(publisher, m, port.ID);
        state = BIState.WAIT_Set;
        if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now.doubleValue() + "] wait R2R");
        waitR2Rport(); //wait here for the answer to be arrived
        if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now.doubleValue() + "] pass R2R");
        state = BIState.RUNNING;
    }

    /**
     * Low level method to send the JSON packet to the target simulation unit
     * @param socket Current queue socket on which the JSON must be published
     * @param m Message to publish on the queue
     * @param topic Topic on which the message must be published
     */
    protected void sendMessage(ZMQ.Socket socket, Message m, String topic) {
//       if (CONFIG.showDebugMessage)   System.out.println("["+ID+"@" +now.doubleValue()+"] send message "+topic);
        try {
            socket.sendMore(topic);
            //Creating the ObjectMapper object
            ObjectMapper mapper = new ObjectMapper();
            //Converting the Object to JSONString
            String jsonString = mapper.writeValueAsString(m);
            socket.send(jsonString);
//            System.out.println("Packet " + jsonString + " sent from " + m.source + " on " + topic);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The most important method in the framework: the actual implementation of the distributed algorithm.
     */
    public void algorithm() {
        Action currentAction = null;
        StopCondition sc = null;
        CoordinationPredicate actualPredicate;

        initiatorsPredicate = setInitiatorsPredicate();

        // Update the TH for all local Initiator ports
        initiatorPorts.forEach(port -> {
            if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now + "] send updateTH on " + port.ID);
            updateTH(port, now);
        });


        state = BIState.RUNNING;
        if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now + "] Running");
        while (!model.simulationIsTerminated() && state != BIState.TERMINATING) {
            currentAction = getNextActionInTodoList();
            if (CONFIG.showDebugMessage)
                System.out.println("[" + ID + "@" + now + "] currentAction= " + currentAction.typeOfAction + " " + currentAction.port + "@" + currentAction.temporalHorizon);

            if (currentAction.typeOfAction == Action.TypeOfAction.TERMINATE || state == BIState.TERMINATING)
                break;

            // ---------------------------------------------------------------------------------------------------
            // If the model is already on sync with the time of the current action, then it is possible to execute
            // the action
            // ---------------------------------------------------------------------------------------------------
            if (now.compareTo(currentAction.temporalHorizon) == 0) {
                if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now + "] call onSync()");
                onSync(currentAction);
                continue;
            }
                actualStepSize = computeNextStepSize(currentAction);

                if ((actualStepSize.compareTo(BigDecimal.valueOf(0.0)) == 0) //no progress in time
                        && currentAction.temporalHorizon.compareTo(now) > 0 //nothing to do at that time but waiting
                ) { //wait new TH
                    if (CONFIG.showDebugMessage)
                        System.out.println("[" + ID + "@" + now + "] nothing to do, wait new TH");
                    todo.add(currentAction); // Reschedule current action on the queue
                    // wait that all the port I follow have put their TH
                    waitTH();
                    if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now + "] TH OK");
                    continue;
                }

                TemporalPredicate nextTimeToStopPredicate = new TemporalPredicate(actualStepSize.intValueExact());

                if (followerPorts.isEmpty() && followerDeterministicPorts.isEmpty()) {
                    nextTimeToStopPredicate = new TemporalPredicate(CONFIG.InfOfSimulation.intValueExact());
                }

                if (initiatorsPredicate != null) {
                    actualPredicate = new BinaryPredicate(nextTimeToStopPredicate, initiatorsPredicate, BinaryPredicate.BooleanBinaryOperator.OR);
                } else {
                    actualPredicate = nextTimeToStopPredicate;
                }

                // If the currentAction can be reached Execute the doStep method and get the StopReason

//                synchronized (lock4now) {
                    sc = model.doStep(actualPredicate);
//                }
                if (CONFIG.showDebugMessage)
                    System.out.println("[" + ID + "@" + now + "] Performed step to " + sc.timeValue + " with " + actualPredicate.toString());


                // Retrieve the current internal time, it will use to determine the actual stop reason
                now = Utils.toBigDecimal(sc.timeValue);
                if (CONFIG.showDebugMessage)
                    System.out.println("[" + ID + "@" + now + "] Performed step stop due to " + sc.stopReason);

                if (now.compareTo(CONFIG.EndOfSimulation) >= 0) {
                    if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now + "] Terminating ");
                    break;
                }

                // In FMI 2.0, it is possible to have only a Temporal Condition as a Stop Reason
                // The timestamp of the current action is aligned with the internal time of the FMU
                // Add here the error specified in the sync statement if any
                if (sc.stopReason == StopReason.TIME) {
                    if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now + "] call onTime()");
                    onTime(currentAction, sc);
                } else if (sc.stopReason == StopReason.EVENT) {
                    if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now + "] call onEvent()");
                    onEvent(currentAction, sc);
                } else if (sc.stopReason == StopReason.READYTOREAD) {
                    if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now + "] call onR2R()");
                    onReadyToRead(currentAction, sc);
                } else if (sc.stopReason == StopReason.UPDATE) {
                    if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now + "] call onUpdate()");
                    onUpdated(currentAction, sc);
                }
                if ((!currentAction.isDone())) {
                    todo.add(currentAction); //was not done this time, reschedule.
                }
        }
        if (CONFIG.showDebugMessage) System.out.println("[" + ID + "@" + now.doubleValue() + "] Exit from main loop ");
        onEnd();
        // Message all the connected models that the simulation is terminating
        followerDeterministicPorts.forEach(
                port -> {
                    Message m = new Message(Action.TypeOfAction.TERMINATE, port.ID, "tcp://" + hostname + ":" + this.port, CONFIG.EndOfSimulation);
                    sendMessage(publisher, m, port.ID);
                });
        initiatorPorts.forEach(
                port -> {
                    Message m = new Message(Action.TypeOfAction.TERMINATE, port.ID, "tcp://" + hostname + ":" + this.port, CONFIG.EndOfSimulation);
                    sendMessage(publisher, m, port.ID);
                });
        followerPorts.forEach(
                port -> {
                    Message m = new Message(Action.TypeOfAction.TERMINATE, port.ID, "tcp://" + hostname + ":" + this.port, CONFIG.EndOfSimulation);
                    sendMessage(publisher, m, port.ID);
                });

    }

    /**
     * Given the current action, it computes the smaller step size that the simulation unit can safely perform
     * @param currentAction The current action retrieved from the to-do queue
     * @return The step size that the simulation unit can safely perform without violating the synchronization constraints and the triggering conditions
     */
    private BigDecimal computeNextStepSize(Action currentAction) {
        if (initiatorsTemporalHorizons.size() < numberOfInitiatorsToWaitfor) {
            return new BigDecimal(0.0); //no way to advance in time
        }
        if (!followerDeterministicPorts.isEmpty()) {
            // Compute the nextDeterministicTemporalHorizon
            nextDeterministicTemporalHorizon = now.add(Utils.getDeterministicMinNextTH(followerDeterministicPorts, now));

            ConcurrentHashMap<String, BigDecimal> initiatorsTemporalHorizonWithoutDeterministics = removeDeterminsticFromInitiatorTH();
            if (!initiatorsTemporalHorizonWithoutDeterministics.isEmpty()) {
                nextInitiatorsTemporalHorizon = initiatorsTemporalHorizonWithoutDeterministics.values().stream().min(BigDecimal::compareTo).get();
                // Compute the nextTimeToStop

                nextTimeToStop = nextDeterministicTemporalHorizon.min(currentAction.temporalHorizon).min(nextInitiatorsTemporalHorizon);
            } else {
                nextTimeToStop = nextDeterministicTemporalHorizon.min(currentAction.temporalHorizon);
            }
        } else {
            nextTimeToStop = currentAction.temporalHorizon;
        }
        // Compute the actual step size
        BigDecimal res = nextTimeToStop.subtract(now);
        assert (res.compareTo(BigDecimal.valueOf(0.0)) >= 0); //ensure no regression in time
        return res;
    }

    /**
     * Retrieve from the to-do queue the next action to perform on the simulation unit
     * @return The action to evaluate and perform
     */
    private Action getNextActionInTodoList() {
        Action currentAction = null;
        if (followerPorts.isEmpty() && followerDeterministicPorts.isEmpty() && todo.isEmpty()) { //! initiatorsPredicate.contains(StopReason.READYTOREAD, null, null, null)) {
            // The model has only initiator ports
            currentAction = new ActionWithPredicate(CONFIG.EndOfSimulation);
        } else {
            // proceed with the rest, All temporal horizons are set
            try {
                currentAction = todo.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return currentAction;
    }

    /**
     * Remove the deterministic initiators from the list of all connected initiators
     * @return HashMap containg the map with the connected initiators and their current temporal horizion
     */
    private ConcurrentHashMap<String, BigDecimal> removeDeterminsticFromInitiatorTH() {
        ConcurrentHashMap<String, BigDecimal> initiatorsTemporalHorizonWithoutDeterministics = new ConcurrentHashMap<String, BigDecimal>(initiatorsTemporalHorizons);
        for (Port dp : followerDeterministicPorts) {
            initiatorsTemporalHorizonWithoutDeterministics.remove(dp.getModelID());
        }
        return initiatorsTemporalHorizonWithoutDeterministics;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Each input port has its own thread to subscribe to the corresponding output port
    // -----------------------------------------------------------------------------------------------------------------
    protected class SubscriptionService implements Runnable {
        String target;
        String topic;
        ZMQ.Socket subscriber;
        ObjectMapper mapper = new ObjectMapper();
        String syncClientHostname;

        public SubscriptionService(String target, String topic, String syncClientHostname) {
            // this.subscriber = context.createSocket(SocketType.SUB);
            this.target = target;
            this.topic = topic;
            this.syncClientHostname = syncClientHostname;
        }

        @Override
        public void run() {
            try {
                if (CONFIG.showDebugMessage)
                    System.out.println("[" + ID + "@" + now + "] Connecting to " + topic + " on " + target);
                subscriber = context.createSocket(SocketType.SUB);
                if (!subscriber.connect(target)) {
                    System.out.println("Error connecting");
                }
                if (CONFIG.showDebugMessage)
                    System.out.println("[" + ID + "@" + now + "] Subscribing to " + topic + " on " + target);

                subscriber.subscribe(topic.getBytes(ZMQ.CHARSET));
            } catch (ZMQException e) {
                System.out.println("ZMQException " + e.toString());
            }
            if (CONFIG.showDebugMessage)
                System.out.println("[" + ID + "@" + now + "] Synchronizing on " + syncClientHostname);
            sync(syncClientHostname);
            if (CONFIG.showDebugMessage)
                System.out.println("[" + ID + "@" + now + "] Waiting for " + topic + " on " + target);

            while (!Thread.currentThread().isInterrupted()) {
                // Read envelope with address
                String address = subscriber.recvStr();
                // Read message contents
                String contents = subscriber.recvStr();
//                System.out.println(address + " : " + contents);

//                synchronized (lock4now) {
                    if (now.compareTo(CONFIG.EndOfSimulation) >= 0) {
                        state = BIState.TERMINATING;
                        todo.add(new Action(Action.TypeOfAction.TERMINATE, CONFIG.StartOfSimulation));
                        break;
                    }

                    // Now we have two types of messages: Action and UpdateTH
                    Message m = null;
                    try {
                        SimpleModule module = new SimpleModule();
                        module.addDeserializer(Message.class, new MessageDeserializer(Message.class));
                        mapper.registerModule(module);
                        if (CONFIG.showDebugMessage)
                            System.out.println("[" + ID + "@" + now + "] Message " + contents);
                        m = mapper.readValue(contents, Message.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    assert m != null;
                    if (CONFIG.showDebugMessage)
                        System.out.println("[" + ID + "@" + now.doubleValue() + "] receive message " + topic);

                    Action a = new ActionOnPort(m.typeOfAction, portMap.get(m.id), this.target, m.temporalHorizon, m.value);

                    if (m.typeOfAction == Action.TypeOfAction.TERMINATE) {
                        if (CONFIG.showDebugMessage)
                            System.out.println("[" + ID + "@" + now.doubleValue() + "] receive TERMINATE ");
                        todo.add(new Action(Action.TypeOfAction.TERMINATE, CONFIG.StartOfSimulation));
                        state = BIState.TERMINATING;
                        break;
                    }

                    if (m.temporalHorizon.compareTo(now) < 0 && m.typeOfAction != Action.TypeOfAction.UPDATETH) {
                        System.err.println("[" + ID + "@" + now + "] message referring to the past ! -> " + "[" + ID + "@" + now + "] received " + m.typeOfAction + " " + m.id + " from " + portMap.get(m.id).getModelID() + " with time " + m.temporalHorizon);
                        continue;
                    }

                    switch (m.typeOfAction) {
                        case SET:
                        case RETRIEVE:
                        case FAKE:
                        case PUBLISH:
                            if (CONFIG.showDebugMessage)
                                System.out.println("[" + ID + "@" + now + "] Receive: " + m.typeOfAction + " " + m.id + " @ " + m.temporalHorizon + " from " + a.port.getModelID());
                            todo.add(a);
                            if (state == BIState.WAIT_Set) {
                                notifyR2R();
                            }
                        case UPDATETH:
                            if (CONFIG.showDebugMessage)
                                System.out.println("[" + ID + "@" + now + "] received a TH of " + m.temporalHorizon + " from " + portMap.get(m.id).getModelID());
                            if (followerPorts.contains(portMap.get(m.id)) || followerDeterministicPorts.contains(portMap.get(m.id))) {
                                // TODO Update only if the value is greater (delayed messages) or ROLLBACK!
                                // Update only if the initiatorsTemporalHorizons does not contain a corresponding entry or the entry is outdated
                                if (!initiatorsTemporalHorizons.containsKey(portMap.get(m.id).getModelID()) || initiatorsTemporalHorizons.get(portMap.get(m.id).getModelID()).compareTo(m.temporalHorizon) <= 0) {
                                    initiatorsTemporalHorizons.put(portMap.get(m.id).getModelID(), m.temporalHorizon);
                                    if (CONFIG.showDebugMessage)
                                        System.out.println("[" + ID + "@" + now + "] add TH of " + portMap.get(m.id).getModelID() + " with " + m.temporalHorizon);
//                               if (CONFIG.showDebugMessage)   System.out.println("["+ID+"@"+now+"] initiatorsTemporalHorizons.size() == numberOfInitiatorsToWaitfor       " +initiatorsTemporalHorizons.size() +"=="+ numberOfInitiatorsToWaitfor);
//                                if(initiatorsTemporalHorizons.size() == numberOfInitiatorsToWaitfor) {
//                                   if (CONFIG.showDebugMessage)   System.out.println("["+ID+"@"+now+"] call notifyTH()");
                                    notifyTH();
//                                }
                                }
                            }
                            break;
                    }
//                }
            }
        }

        private void sync(String syncHostname) {
            try {
                //  Second, synchronize with publisher
                ZMQ.Socket syncclient = context.createSocket(SocketType.REQ);
                syncclient.connect(syncHostname);

                //  - send a synchronization request
                syncclient.send("CanI?".getBytes(ZMQ.CHARSET), 0);

                //  - wait for synchronization reply
                syncclient.recv(0);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
}
