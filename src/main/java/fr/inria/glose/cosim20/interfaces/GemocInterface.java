package fr.inria.glose.cosim20.interfaces;

import org.eclipse.gemoc.execution.commons.commands.*;
import org.eclipse.gemoc.execution.commons.predicates.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public class GemocInterface implements Interface {
	Socket socket = null;
	ObjectOutputStream cout = null;
	ObjectInputStream  cin  = null;
	Process controllerExec = null;
	OutputStream out = null;

	public GemocInterface(String path, String host, int port) {
		try {
			controllerExec = Runtime.getRuntime().exec("java -jar " + path);
			Thread.sleep(5000);
			socket = new Socket(host, port);
			cout = new ObjectOutputStream(socket.getOutputStream());
			cin = new ObjectInputStream(socket.getInputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public StopCondition doStep(CoordinationPredicate p) {
		DoStepCommand doStep = new DoStepCommand(p);
		try {
			cout.writeObject(doStep);
			StopCondition sc = (StopCondition) cin.readObject();
			return sc;
		} catch (Exception  e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object get(String varQN){
		GetVariableCommand getVar = new GetVariableCommand(varQN);
		try {
			cout.writeObject(getVar);
			Object varValue = (Object) cin.readObject();
			return varValue;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Boolean set(String varQN, Object newValue){ //work only with integer value for now
		SetVariableCommand setVar = new SetVariableCommand(varQN, new Integer(newValue.toString()));
		Boolean resValue = false;
		try {
			cout.writeObject(setVar);
			resValue = (Boolean) cin.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return resValue;
	}

	@Override
	public Boolean simulationIsTerminated() {
		return false;
	}

	public void terminate(){
		StopCommand stop = new StopCommand();
		try {
			cout.writeObject(stop);
			socket.close();
			Thread.sleep(500);
			controllerExec.destroy();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		terminate();
	}
}
