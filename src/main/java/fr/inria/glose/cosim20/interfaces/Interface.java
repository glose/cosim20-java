package fr.inria.glose.cosim20.interfaces;

import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;

public interface Interface {
    public StopCondition doStep(CoordinationPredicate predicate);
    public Object get(String variableName);
    public Boolean set(String variableName, Object value);
    public Boolean simulationIsTerminated();
}
