package fr.inria.glose.cosim20.interfaces;

import fr.inria.glose.cosim20.CONFIG;
import fr.inria.glose.cosim20.Utils;
import org.eclipse.gemoc.execution.commons.commands.*;
import org.eclipse.gemoc.execution.commons.predicates.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.Socket;

public class FMI2GemocInterface implements Interface {
    Socket socket = null;
    ObjectOutputStream cout = null;
    ObjectInputStream  cin  = null;
    Process controllerExec = null;
    OutputStream out = null;
    BinaryPredicate bp;

    BigDecimal externalTime = Utils.toBigDecimal(0);
    BigDecimal internalTime = Utils.toBigDecimal(0);

    public FMI2GemocInterface(String path, String host, int port) {
        try {
            controllerExec = Runtime.getRuntime().exec("java -jar " + path);
            Thread.sleep(2500);
            socket = new Socket(host, port);
            cout = new ObjectOutputStream(socket.getOutputStream());
            cin = new ObjectInputStream(socket.getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public StopCondition doStep(CoordinationPredicate predicate) {
        DoStepCommand doStep = new DoStepCommand(predicate);
        try {
            cout.writeObject(doStep);
            StopCondition sc = (StopCondition) cin.readObject();
            internalTime = Utils.toBigDecimal(sc.timeValue);
            sc.stopReason = StopReason.TIME;
            return sc;
        } catch (Exception  e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Object get(String variableName) {
        GetVariableCommand getVar = new GetVariableCommand(variableName);
        try {
            cout.writeObject(getVar);
            Object varValue = (Object) cin.readObject();
            return varValue;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;    }

    @Override
    public Boolean set(String variableName, Object value) {
        SetVariableCommand setVar = new SetVariableCommand(variableName, new Integer(value.toString()));
        Boolean resValue = false;
        try {
            cout.writeObject(setVar);
            resValue = (Boolean) cin.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return resValue;
    }

    @Override
    public Boolean simulationIsTerminated() {
        return internalTime.compareTo(CONFIG.EndOfSimulation) >= 0;
    }
}
