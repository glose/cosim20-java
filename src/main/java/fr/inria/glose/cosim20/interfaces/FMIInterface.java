package fr.inria.glose.cosim20.interfaces;

import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.commands.StopReason;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;
import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;
import org.javafmi.proxy.Status;
import org.javafmi.wrapper.Simulation;

public class FMIInterface implements Interface {
    private Simulation sim;

    public FMIInterface(String fmuPath, double startTime, double stopTime) {
        this.sim = new Simulation(fmuPath);
        this.sim.init(startTime, stopTime);
    }

    public StopCondition doStep(CoordinationPredicate predicate) {
        TemporalPredicate tp = predicate.getTemporalPredicate();
        // Execute the doStep of FMI 2.0
        Status status = null;
        if(tp.deltaT == 0){
            status = sim.doStep(0.0001);
        }else{
            status = sim.doStep(tp.deltaT);
        }


        if (status == Status.OK) {
            return new StopCondition(StopReason.TIME, "", "", ((int) sim.getCurrentTime()));
        }
        return new StopCondition(StopReason.TIME, "", "", ((int)sim.getCurrentTime()));
    }

    public Object get(String variableName) {
        try {
            Object value = sim.read(variableName).asDouble();
            return value;
        } catch (Exception e) {
            System.out.println("[FMI] Get error: " + e);
        }
        return null;
    }

    @Override
    public Boolean set(String variableName, Object value) {
       if (value instanceof  Integer){
           set(variableName, (Integer)value);
           return true;
       }
        if (value instanceof  Boolean){
            set(variableName, (Boolean)value);
            return true;
        }
        if (value instanceof  Double){
            set(variableName, (Double)value);
            return true;
        }
        return false;
    }

    @Override
    public Boolean simulationIsTerminated() {
        return sim.isTerminated();
    }

    public void set(String variableName, Integer value) {
        try {
            Status status = sim.write(variableName).with(value);
            if (status != Status.OK) {
                throw new Exception("Write failed for " + variableName + " with status " + status);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void set(String variableName, Boolean value) {
        try {
            Status status = sim.write(variableName).with(value);
            if (status != Status.OK) {
                throw new Exception("Write failed for " + variableName + " with status " + status);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void set(String variableName, String value) {
        try {
            Status status = sim.write(variableName).with(value);
            if (status != Status.OK) {
                throw new Exception("Write failed for " + variableName + " with status " + status);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void set(String variableName, Double value) {
        try {
            Status status = sim.write(variableName).with(value);
            if (status != Status.OK) {
               throw new Exception("Write failed for " + variableName + " with status " + status);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
