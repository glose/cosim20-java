package fr.inria.glose.cosim20;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.zeromq.ZMQ;

import java.math.BigDecimal;

public class ActionOnPort extends Action{

    public ActionOnPort(TypeOfAction typeOfAction, Port source, String hostSource, BigDecimal temporalHorizon) {
        super(typeOfAction, temporalHorizon);
        this.done = false;
        this.port = source;
        this.hostSource = hostSource;
    }

    public ActionOnPort(TypeOfAction typeOfAction, Port source, String hostSource, BigDecimal temporalHorizon, Object value) {
        super(typeOfAction, temporalHorizon);
        this.done = false;
        this.port = source;
        this.value = value;
        this.hostSource = hostSource;
    }

    public void replyWith(ZMQ.Socket socket, BigDecimal temporalHorizon, Object value) {
        socket.sendMore(port.topic);
        Message m = new Message(TypeOfAction.SET, port.ID, hostSource, temporalHorizon, value);
        // Creating the ObjectMapper object
        ObjectMapper mapper = new ObjectMapper();
        // Converting the Object to JSONString
        try {
            String jsonString = mapper.writeValueAsString(m);
            System.out.println(jsonString);
            socket.send(jsonString);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
