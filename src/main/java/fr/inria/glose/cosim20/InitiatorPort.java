package fr.inria.glose.cosim20;

public class InitiatorPort extends Port {
    public InitiatorPort(String ID, String modelVarName, String modelID, Object value) {
        super(ID, modelVarName, modelID, value);
    }
}
