package fr.inria.glose.cosim20;

public class FollowerPort extends Port {
    public FollowerPort(String ID, String modelVarName, String modelID, Object value) {
        super(ID, modelVarName, modelID, value);
    }
}
