package fr.inria.glose.cosim20;

import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;

import java.math.BigDecimal;

public class PeriodicAction extends Action {
    TemporalPredicate temporalPredicate;

    // A periodic action can publish or set a variable
    public PeriodicAction(TypeOfAction actionType, Port source,  String hostSource, BigDecimal now, TemporalPredicate temporalPredicate) {
        super(actionType, now.add(BigDecimal.valueOf(temporalPredicate.deltaT)));
        this.temporalPredicate = temporalPredicate;
        this.hostSource = hostSource;
        this.port = source;
    }

    public TemporalPredicate getTemporalPredicate() {
        return temporalPredicate;
    }
}
