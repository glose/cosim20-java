package org.eclipse.gemoc.execution.commons.commands;

import org.eclipse.gemoc.execution.commons.predicates.*;
import java.io.Serializable;

public class DoStepCommand implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public CoordinationPredicate predicate;
	
	public DoStepCommand(CoordinationPredicate pred) {
		predicate = pred;
	}

}
