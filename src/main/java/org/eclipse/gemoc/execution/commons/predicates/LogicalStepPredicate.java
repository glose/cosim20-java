package org.eclipse.gemoc.execution.commons.predicates;


import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.execution.commons.commands.*;

public class LogicalStepPredicate extends CoordinationPredicate{
	
	private static final long serialVersionUID = 1L;
	public int nbStepsRequired = 0;
	
	public LogicalStepPredicate(int nbSteps) {
		nbStepsRequired = nbSteps;
	}

	/**
	 * since it is an event predicate, so a predicate on the current state of the MoCC, it will no be used 
	 * in a callback from K3 and consequently this method is uselees, returning consequently always false
	 * @return false, always
	 */
	@Override
	public boolean contains(StopReason predType, EObject caller, String className, String propertyName) {
		return predType == StopReason.LOGICALSTEP;
	}

	@Override
	public TemporalPredicate getTemporalPredicate() {
		return null;
	}
	
	@Override
	public LogicalStepPredicate getLogicalStepPredicate() {
		return this;
	}
	
}