package org.eclipse.gemoc.execution.commons.helpers;

import org.eclipse.emf.ecore.EObject;

import java.util.ArrayList;
import java.util.List;

public class EcoreQNHelper {
	
	/**
	 * The default separator used between elements of a qualified name for CCSLKernel objects
	 */
	public static final String defaultSeparator = "::";

	/**
	 * Returns the name of the object using the {@link #getSimpleName(EObject)} function.
	 * @param object
	 * @return
	 * @see #getSimpleName(EObject)
	 */
	public static String getName(EObject object) {
		return getSimpleName(object);
	}
	
	/**
	 * Returns the name of the object. Only instances of Object whose class has a "name" field 
	 * have a name. For other objects the function returns <code>null</code>. 
	 * @param object
	 * @return
	 */
	public static String getSimpleName(EObject object) {
		try {
			return (String) object.eGet(object.eClass().getEStructuralFeature("name"));
		}catch (SecurityException | IllegalArgumentException e) {
			return null;
		}
	}

	/**
	 * Returns the fully qualified name of the object, with elements separated by the default separator
	 * ({@value #defaultSeparator})
	 * @param object
	 * @return
	 * @see #getQualifiedName(EObject, String)
	 */
	public static String getQualifiedName(EObject object) {
		return getQualifiedName(object, defaultSeparator);
	}

	/**
	 * Returns the fully qualified name of the object, with elements separated by the given separator argument.
	 * The fully qualified name is obtained by concatenating all the simple names of objects encountered in the
	 * containment hierarchy starting at the given object, up to and <em>excluding</em> the topmost
	 * {@link ClockConstraintSystem} object at the root of the model. Intermediate objects that have no name
	 * are ignored. The names of the objects are computed using the {@link #getSimpleName(EObject)} function.
	 * @param object
	 * @param separator
	 * @return
	 * @see #getSimpleName(EObject)
	 */
	public static String getQualifiedName(EObject object, String separator) {
		if (object == null) {
			return null;
		}
		String name = getSimpleName(object);
		String parentQN = getQualifiedName(object.eContainer(), separator);
		if (parentQN != null) {
			return ( name != null ? parentQN + separator + name : parentQN );
		}
		else
			return name;
	}
	
	/**
	 * To be used with {@link org.eclipse.xtext.naming.QualifiedName#create(List<String>)}
	 * @param object
	 * @return
	 */
	public static List<String> getQualifiedNameSegments(EObject object) {
		List<String> result;
		if (object.eContainer() == null) {
			result = new ArrayList<String>();
		}
		else {
			result = getQualifiedNameSegments(object.eContainer());
		}
		String name = getSimpleName(object);
		result.add(name);
		return result;
	}

}
