package org.eclipse.gemoc.execution.commons.predicates;


import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.execution.commons.commands.*;

public class EventPredicate extends CoordinationPredicate{
	
	private static final long serialVersionUID = 1L;
	public String objectQName;
	public String eventTypeName;
	
	public EventPredicate(String eventTypeName, String objQN) {
		objectQName = objQN;
		this.eventTypeName = eventTypeName;
	}

	@Override
	public boolean contains(StopReason predType, EObject caller, String callerName, String eventTypeName) {
		return predType == StopReason.EVENT 
			&& callerName != null && objectQName.compareTo(callerName) == 0 
			&& eventTypeName != null && eventTypeName.endsWith("_"+this.eventTypeName);
	}

	@Override
	public TemporalPredicate getTemporalPredicate() {
		return null;
	}
	
	@Override
	public LogicalStepPredicate getLogicalStepPredicate() {
		return null;
	}

	@Override
	public String toString() {
		return "event(" +
				objectQName + ':' +
				eventTypeName +
				')';
	}
}