package org.eclipse.gemoc.execution.commons.predicates;

import fr.inria.glose.cosim20.Utils;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.execution.commons.commands.StopReason;

import java.math.BigDecimal;

public class TemporalDoublePredicate extends TemporalPredicate {
    private static final long serialVersionUID = 1L;
    public BigDecimal deltaT;

    public TemporalDoublePredicate(double deltaT) {
        super(Utils.toBigDecimal(deltaT).intValueExact());
        this.deltaT = Utils.toBigDecimal(deltaT);
    }
    @Override
    public boolean contains(StopReason predType, EObject caller, String className, String propertyName) {
        return predType == StopReason.TIME;
    }

    public double getDeltaT() {
        return deltaT.doubleValue();
    }

    @Override
    public TemporalPredicate getTemporalPredicate() {
        return this;
    }

    @Override
    public LogicalStepPredicate getLogicalStepPredicate() {
        return null;
    }
}
