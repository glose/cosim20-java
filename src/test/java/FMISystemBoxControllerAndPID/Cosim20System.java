package FMISystemBoxControllerAndPID;

import fr.inria.glose.cosim20.CONFIG;
import fr.inria.glose.cosim20.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Cosim20System {
    // Define the "infinite" time as the time of the end of simulation plus an Epsilon
    public static void main(String[] args) {
    	CONFIG.showDebugMessage = false;
    	CONFIG.EndOfSimulation = Utils.toBigDecimal(30000);
		try {
		    OscilloscupServer.createAutoRefreshServer(42663, "cosimPlotter", 1000);
		} catch (IOException e) {
		    e.printStackTrace();
		}
    	
    	List<Callable<Object>> calls = new ArrayList<>();
    	CPUinBoxWithFanHeatModel cpuinboxwithfanheatmodel = new CPUinBoxWithFanHeatModel();
    	calls.add(Executors.callable(cpuinboxwithfanheatmodel));
    	OverHeatController overheatcontroller = new OverHeatController();
    	calls.add(Executors.callable(overheatcontroller));
    	FanController fancontroller = new FanController();
    	calls.add(Executors.callable(fancontroller));
        ExecutorService executor = Executors.newFixedThreadPool(calls.size());

        System.out.println("Starting the simulation");
        try {
            executor.invokeAll(calls);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
