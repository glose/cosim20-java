package FMISystemBoxControllerAndPID;

import com.google.common.base.Stopwatch;
import fr.cnrs.i3s.luchogie.oscilloscup.client.OscilloscupClient;
import fr.inria.glose.cosim20.*;
import fr.inria.glose.cosim20.interfaces.FMIInterface;
import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;
import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class FanController extends CoordinationInterface {
	
    //-------------------------------
    //Plotter stuff
    //-------------------------------
    OscilloscupClient plotterClient;
	Stopwatch stopwatch = Stopwatch.createStarted();   
	
	// ----------------------------------------------------------------
	// FMI Standard
	// ----------------------------------------------------------------
	double startTime = 0.0;
	double stopTime = CONFIG.EndOfSimulation.doubleValue();
	String fmuPath = "src/test/resources/FanController.fmu";
	
	public FollowerDeterministicPort CPUTemperature_Box2PID = new FollowerDeterministicPort("CPUTemperature_Box2PID", "CPUTemperature", "CPUinBoxWithFanHeatModel", 0, new TemporalPredicate(1));
	public Port CPUfanSpeed_PID2Box = new InitiatorPort("CPUfanSpeed_PID2Box", "CPUfanSpeed", "CPUinBoxWithFanHeatModel", 0); // OUTPUT
	

   public FanController() {
       super("fanController", "localhost", 43557, 39499, 0, 1);

		// Setup the FMI model
		model = new FMIInterface(fmuPath, startTime, stopTime);
		
	
		try {
		    plotterClient  = new OscilloscupClient("localhost", 42663);
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		        
        initiatorPorts = new ArrayList<>();
        followerPorts = new ArrayList<>();
        followerDeterministicPorts = new ArrayList<>();
        
		followerDeterministicPorts.add(CPUTemperature_Box2PID);
		initiatorPorts.add(CPUfanSpeed_PID2Box);
		todo.add(new PeriodicAction(Action.TypeOfAction.PUBLISH, CPUfanSpeed_PID2Box, "tcp://localhost:43557", now, new TemporalPredicate(1)));

        // The <key> is the id of the external port linked with a port of this model (<value>)
        portMap.put("CPUTemperature_Box2PID", CPUTemperature_Box2PID);
        portMap.put("CPUTemperature", CPUTemperature_Box2PID);
        portMap.put("CPUTemperature", CPUTemperature_Box2PID);
        portMap.put("CPUfanSpeed_PID2Box", CPUfanSpeed_PID2Box);
        portMap.put("CPUfanSpeed", CPUfanSpeed_PID2Box);
        portMap.put("CPUfanSpeed", CPUfanSpeed_PID2Box);

		addNewInputPort("CPUTemperature_Box2PID", "tcp://localhost:44705", "tcp://localhost:37641");
		
		model.set("targetTemperature", 65);
        model.set("Kp", 5.0);
		
		model.set("CPUTemperature", 25);
		  }

    @Override
    public void onTime(Action currentAction, StopCondition sr) {
        if (now.compareTo(currentAction.temporalHorizon) == 0) {
        	// Execute the corresponding action
        	if (currentAction.port.compareTo(CPUTemperature_Box2PID) == 0) {
        		double value = (double) currentAction.getValue();
        		
        		model.set("CPUTemperature", value);
        		if (CONFIG.showDebugMessage) System.out.println("["+ID+"] CPUTemperature = " + model.get("CPUTemperature") + " @ " + now.doubleValue());
        	}
        	// Execute the corresponding action
        	if (currentAction.port.compareTo(CPUfanSpeed_PID2Box) == 0) {
                double value = (double) model.get(CPUfanSpeed_PID2Box.associatedModelVariableName);
                publish(CPUfanSpeed_PID2Box, value, now);
            }

			// Extra plot to show the actual data nature of the variable
            // Remove the current action from the to-do list
            currentAction.setDone();

            if (currentAction instanceof PeriodicAction) {
                todo.add(new PeriodicAction(currentAction.typeOfAction, currentAction.port, currentAction.hostSource, now, ((PeriodicAction) currentAction).getTemporalPredicate()));
            }
        }
    }

    @Override
    public void onEvent(Action currentAction, StopCondition sc) {
    	
    	
    }

    @Override
    public void onReadyToRead(Action currentAction, StopCondition sc) {
    }

    @Override
    public void onUpdated(Action currentAction, StopCondition sc) {
		// Not implemented yet
    }

    @Override
    public void onSync(Action currentAction) {
    	// Statically generated ONLY if the BI abstracts a FMU
    	model.doStep(new TemporalPredicate(0));
		onTime(currentAction, null);
    }

    @Override
    public CoordinationPredicate setInitiatorsPredicate() {
    	
    	
    	return null;			        
    }
		        @Override
		        public void onEnd() {
	
	stopwatch.stop(); // optional
	System.out.println("Time elapsed: "+ stopwatch.elapsed(TimeUnit.SECONDS));
	
		        }
        
}
