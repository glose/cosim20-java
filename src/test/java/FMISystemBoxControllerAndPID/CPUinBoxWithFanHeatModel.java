package FMISystemBoxControllerAndPID;

import com.google.common.base.Stopwatch;
import fr.cnrs.i3s.luchogie.oscilloscup.client.OscilloscupClient;
import fr.inria.glose.cosim20.*;
import fr.inria.glose.cosim20.interfaces.FMIInterface;
import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;
import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class CPUinBoxWithFanHeatModel extends CoordinationInterface {

    //-------------------------------
    //Plotter stuff
    //-------------------------------
    OscilloscupClient plotterClient;
    Stopwatch stopwatch = Stopwatch.createStarted();

    // ----------------------------------------------------------------
    // FMI Standard
    // ----------------------------------------------------------------
    double startTime = 0.0;
    double stopTime = CONFIG.EndOfSimulation.doubleValue();
    String fmuPath = "src/test/resources/CPUinBoxWithFanHeatModel.fmu";

    public FollowerDeterministicPort CPUfanSpeed_PID2Box = new FollowerDeterministicPort("CPUfanSpeed_PID2Box", "CPUfanSpeed", "fanController", 0, new TemporalPredicate(1));


    public Port CPUTemperature_Box2Controller = new InitiatorPort("CPUTemperature_Box2Controller", "CPUTemperature", "overHeatController", 0.0); // OUTPUT
    public FollowerDeterministicPort isStopped_Controller2Box = new FollowerDeterministicPort("isStopped_Controller2Box", "isStopped", "overHeatController", 0, new TemporalPredicate(5));
    public Port CPUTemperature_Box2PID = new InitiatorPort("CPUTemperature_Box2PID", "CPUTemperature", "fanController", 0.0); // OUTPUT

    PrintWriter writer4cputemperature;
    PrintWriter writer4isstopped;

    int counterTemp = 0;
    int counterCPU = 0;

    public CPUinBoxWithFanHeatModel() {
        super("CPUinBoxWithFanHeatModel", "localhost", 44705, 37641, 0, 2);

        // Setup the FMI model
        model = new FMIInterface(fmuPath, startTime, stopTime);


        try {
            plotterClient = new OscilloscupClient("localhost", 42663);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            writer4cputemperature = new PrintWriter(new File("CPUinBoxWithFanHeatModel_cputemperature.csv"));
            writer4cputemperature.write("time,CPUTemperature\n");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            writer4isstopped = new PrintWriter(new File("CPUinBoxWithFanHeatModel_isstopped.csv"));
            writer4isstopped.write("time,isStopped\n");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        initiatorPorts = new ArrayList<>();
        followerPorts = new ArrayList<>();
        followerDeterministicPorts = new ArrayList<>();

        initiatorPorts.add(CPUTemperature_Box2Controller);
        todo.add(new PeriodicAction(Action.TypeOfAction.PUBLISH, CPUTemperature_Box2Controller, "tcp://localhost:44705", now, new TemporalPredicate(5)));
        followerDeterministicPorts.add(isStopped_Controller2Box);
        initiatorPorts.add(CPUTemperature_Box2PID);
        todo.add(new PeriodicAction(Action.TypeOfAction.PUBLISH, CPUTemperature_Box2PID, "tcp://localhost:44705", now, new TemporalPredicate(1)));
        followerDeterministicPorts.add(CPUfanSpeed_PID2Box);

        // The <key> is the id of the external port linked with a port of this model (<value>)
        portMap.put("CpuTemperature_Box2Controller", CPUTemperature_Box2Controller);
        portMap.put("CPUTemperature", CPUTemperature_Box2Controller);
        portMap.put("SwitchCPUState_Controller2Box", isStopped_Controller2Box);
        portMap.put("isStopped", isStopped_Controller2Box);
        portMap.put("CPUTemperature_Box2PID", CPUTemperature_Box2PID);
        portMap.put("CPUTemperature", CPUTemperature_Box2PID);
        portMap.put("CPUfanSpeed_PID2Box", CPUfanSpeed_PID2Box);
        portMap.put("CPUfanSpeed", CPUfanSpeed_PID2Box);

        addNewInputPort("SwitchCPUState_Controller2Box", "tcp://localhost:42679", "tcp://localhost:38397");
        addNewInputPort("CPUfanSpeed_PID2Box", "tcp://localhost:43557", "tcp://localhost:39499");


        model.set("isStopped", false);
        model.set("CPUfanSpeed", 0);
    }

    @Override
    public void onTime(Action currentAction, StopCondition sr) {
        if (now.compareTo(currentAction.temporalHorizon) == 0) {
            // Execute the corresponding action
            if (currentAction.port.compareTo(CPUTemperature_Box2Controller) == 0) {
                double value = (double) model.get(CPUTemperature_Box2Controller.associatedModelVariableName);

                publish(CPUTemperature_Box2Controller, value, now);
                try {
                    plotterClient.addPoint("cosimPlotter", 0, now.doubleValue(), value);
                    writer4cputemperature.write(now.doubleValue() + "," + value + "\n");
                    writer4cputemperature.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            // Execute the corresponding action
            if (currentAction.port.compareTo(isStopped_Controller2Box) == 0) {
                boolean value = (boolean) currentAction.getValue();

                model.set("isStopped", value);
                if (CONFIG.showDebugMessage)
                    System.out.println("[" + ID + "] isStopped = " + model.get("isStopped") + " @ " + now.doubleValue());
                try {
                    plotterClient.addPoint("cosimPlotter", 1, now.doubleValue(), ((boolean) currentAction.getValue() == false) ? 0 : 20);
                    writer4isstopped.write(now.doubleValue() + "," + currentAction.getValue() + "\n");
                    writer4isstopped.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // Execute the corresponding action
            if (currentAction.port.compareTo(CPUTemperature_Box2PID) == 0) {
                double value = (double) model.get(CPUTemperature_Box2PID.associatedModelVariableName);

                publish(CPUTemperature_Box2PID, value, now);
                try {
                    plotterClient.addPoint("cosimPlotter", 2, now.doubleValue(), value);
                    // writer4cputemperature.write(now.doubleValue() + "," + value + "\n");
                    // writer4cputemperature.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            // Execute the corresponding action
            if (currentAction.port.compareTo(CPUfanSpeed_PID2Box) == 0) {
                double value = (double) currentAction.getValue();

                model.set("CPUfanSpeed", value);
                if (CONFIG.showDebugMessage)
                    System.out.println("[" + ID + "] CPUfanSpeed = " + model.get("CPUfanSpeed") + " @ " + now.doubleValue());
            }

            // Extra plot to show the actual data nature of the variable
            try {
                plotterClient.addPoint("cosimPlotter", 1, now.doubleValue(), (double) model.get(isStopped_Controller2Box.associatedModelVariableName) * 20);
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Remove the current action from the to-do list
            currentAction.setDone();

            if (currentAction instanceof PeriodicAction) {
                todo.add(new PeriodicAction(currentAction.typeOfAction, currentAction.port, currentAction.hostSource, now, ((PeriodicAction) currentAction).getTemporalPredicate()));
            }
        }
    }

    @Override
    public void onEvent(Action currentAction, StopCondition sc) {
    }

    @Override
    public void onReadyToRead(Action currentAction, StopCondition sc) {
    }

    @Override
    public void onUpdated(Action currentAction, StopCondition sc) {
        // Not implemented yet
    }

    @Override
    public void onSync(Action currentAction) {
        // Statically generated ONLY if the BI abstracts a FMU
        model.doStep(new TemporalPredicate(0));
        onTime(currentAction, null);
    }

    @Override
    public CoordinationPredicate setInitiatorsPredicate() {


        return null;
    }

    @Override
    public void onEnd() {
        writer4cputemperature.flush();
        writer4isstopped.flush();

        stopwatch.stop(); // optional
        System.out.println("Time elapsed: " + stopwatch.elapsed(TimeUnit.SECONDS));

    }

}
