package FMISystemBoxControllerAndPID;

import com.google.common.base.Stopwatch;
import fr.cnrs.i3s.luchogie.oscilloscup.client.OscilloscupClient;
import fr.inria.glose.cosim20.*;
import fr.inria.glose.cosim20.interfaces.GemocInterface;
import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;
import org.eclipse.gemoc.execution.commons.predicates.EventPredicate;
import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class OverHeatController extends CoordinationInterface {
	
    //-------------------------------
    //Plotter stuff
    //-------------------------------
    OscilloscupClient plotterClient;
	Stopwatch stopwatch = Stopwatch.createStarted();   
	
	
	public FollowerDeterministicPort CpuTemperature_Box2Controller = new FollowerDeterministicPort("CpuTemperature_Box2Controller", "CPUprotection::cpuTemperature", "CPUinBoxWithFanHeatModel", 0, new TemporalPredicate(5));
	public Port SwitchCPUState_Controller2Box = new InitiatorPort("SwitchCPUState_Controller2Box", "CPUprotection::switchCPUState", "CPUinBoxWithFanHeatModel", false); // OUTPUT
	

   public OverHeatController() {
       super("overHeatController", "localhost", 42679, 38397, 0, 1);

		
		// -------------------------------------------------------
		// Initialize the model
		// -------------------------------------------------------
		this.model = new GemocInterface("src/test/resources/overHeatControler.jar", "localhost", 39635);
	
		try {
		    plotterClient  = new OscilloscupClient("localhost", 42663);
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		        
        initiatorPorts = new ArrayList<>();
        followerPorts = new ArrayList<>();
        followerDeterministicPorts = new ArrayList<>();
        
		followerDeterministicPorts.add(CpuTemperature_Box2Controller);
		initiatorPorts.add(SwitchCPUState_Controller2Box);
		todo.add(new PeriodicAction(Action.TypeOfAction.PUBLISH, SwitchCPUState_Controller2Box, "tcp://localhost:42679", now, new TemporalPredicate(5)));

        // The <key> is the id of the external port linked with a port of this model (<value>)
        portMap.put("CPUTemperature_Box2Controller", CpuTemperature_Box2Controller);
        portMap.put("CpuTemperature", CpuTemperature_Box2Controller);
        portMap.put("CPUprotection::cpuTemperature", CpuTemperature_Box2Controller);
        portMap.put("isStopped_Controller2Box", SwitchCPUState_Controller2Box);
        portMap.put("SwitchCPUState", SwitchCPUState_Controller2Box);
        portMap.put("CPUprotection::switchCPUState", SwitchCPUState_Controller2Box);

		addNewInputPort("CPUTemperature_Box2Controller", "tcp://localhost:44705", "tcp://localhost:37641");
		
		
		model.set("CPUprotection::cpuTemperature::currentValue", 25);
		  }

    @Override
    public void onTime(Action currentAction, StopCondition sr) {
        if (now.compareTo(currentAction.temporalHorizon) == 0) {
        	// Execute the corresponding action
        	if (currentAction.port.compareTo(CpuTemperature_Box2Controller) == 0) {
        		BigDecimal temp = new BigDecimal(currentAction.getValue().toString());
        		int value = temp.intValue();
        		
        		model.set("CPUprotection::cpuTemperature::currentValue", value);
        		if (CONFIG.showDebugMessage) System.out.println("["+ID+"] CpuTemperature = " + model.get("CPUprotection::cpuTemperature::currentValue") + " @ " + now.doubleValue());
        	}
        	// Execute the corresponding action
        	if (currentAction.port.compareTo(SwitchCPUState_Controller2Box) == 0) {
        			boolean value = (boolean) SwitchCPUState_Controller2Box.getValue();
        		
        		publish(SwitchCPUState_Controller2Box, value, now);
				        		
        	}

			// Extra plot to show the actual data nature of the variable
            // Remove the current action from the to-do list
            currentAction.setDone();

            if (currentAction instanceof PeriodicAction) {
                todo.add(new PeriodicAction(currentAction.typeOfAction, currentAction.port, currentAction.hostSource, now, ((PeriodicAction) currentAction).getTemporalPredicate()));
            }
        }
    }

    @Override
    public void onEvent(Action currentAction, StopCondition sc) {
    	// If there is a Port defined as Transient, then save its changements of state
    	SwitchCPUState_Controller2Box.setValue(!((boolean)SwitchCPUState_Controller2Box.getValue()));
    	System.out.println("--> An event occurs in OverHeatController@" + now);
    }

    @Override
    public void onReadyToRead(Action currentAction, StopCondition sc) {
    }

    @Override
    public void onUpdated(Action currentAction, StopCondition sc) {
		// Not implemented yet
    }

    @Override
    public void onSync(Action currentAction) {
		onTime(currentAction, null);
    }

    @Override
    public CoordinationPredicate setInitiatorsPredicate() {

    	  return new EventPredicate("occurs", "CPUprotection::switchCPUState");
    }
		        @Override
		        public void onEnd() {
	
	stopwatch.stop(); // optional
	System.out.println("Time elapsed: "+ stopwatch.elapsed(TimeUnit.SECONDS));
	
		        }
        
}
