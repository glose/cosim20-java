package CPUCoolingSystemWithInjectedFanSpeed;

import fr.inria.glose.cosim20.Utils;
import fr.inria.glose.cosim20.CONFIG;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.io.IOException;

public class CPUCoolingSystemWithInjectedFanSpeed {
  // Define the "infinite" time as the time of the end of simulation plus an Epsilon
  public static void main(String[] args) {
    CONFIG.showDebugMessage = true;
    CONFIG.EndOfSimulation = Utils.toBigDecimal(30000);
    try {
      OscilloscupServer.createAutoRefreshServer(37143, "cosimPlotter", 1000);
    } catch (IOException e) {
      e.printStackTrace();
    }

    List<Callable<Object>> calls = new ArrayList<>();
    CPUinBox cpuinbox = new CPUinBox();
    calls.add(Executors.callable(cpuinbox));
    OverHeatController overheatcontroller = new OverHeatController();
    calls.add(Executors.callable(overheatcontroller));
    ExecutorService executor = Executors.newFixedThreadPool(calls.size());

    System.out.println("Starting the simulation");
    try {
      executor.invokeAll(calls);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
