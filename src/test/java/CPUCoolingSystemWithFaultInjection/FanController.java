package CPUCoolingSystemWithFaultInjection;

import fr.inria.glose.cosim20.*;
import fr.inria.glose.cosim20.interfaces.*;
import org.eclipse.gemoc.execution.commons.commands.*;
import org.eclipse.gemoc.execution.commons.predicates.*;
import fr.cnrs.i3s.luchogie.oscilloscup.client.OscilloscupClient;
import java.io.IOException;
import fr.inria.glose.cosim20.CONFIG;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import com.google.common.base.Stopwatch;

public class FanController extends CoordinationInterface {

  // -------------------------------
  // Plotter stuff
  // -------------------------------
  OscilloscupClient plotterClient;
  Stopwatch stopwatch = Stopwatch.createStarted();

  // ----------------------------------------------------------------
  // FMI Standard
  // ----------------------------------------------------------------
  double startTime = 0.0;
  double stopTime = CONFIG.EndOfSimulation.doubleValue();
  String fmuPath =
      "/home/gliboni/runtime-EclipseApplication/Demo-CPUCoolingSystem/su/FanController.fmu";

  public Port CPUfanSpeed_fan2box =
      new InitiatorPort("CPUfanSpeed_fan2box", "CPUfanSpeed", "CPUinBox", 0); // OUTPUT
  public FollowerDeterministicPort CPUTemperature_CPU2fanController =
      new FollowerDeterministicPort(
          "CPUTemperature_CPU2fanController",
          "CPUTemperature",
          "CPUinBox",
          0,
          new TemporalPredicate(5));

  public FanController() {
    super("fanController", "localhost", 35835, 43291, 0, 1);

    // Setup the FMI model
    model = new FMIInterface(fmuPath, startTime, stopTime);

    try {
      plotterClient = new OscilloscupClient("localhost", 40273);
    } catch (IOException e) {
      e.printStackTrace();
    }

    initiatorPorts = new ArrayList<>();
    followerPorts = new ArrayList<>();
    followerDeterministicPorts = new ArrayList<>();

    initiatorPorts.add(CPUfanSpeed_fan2box);
    todo.add(
        new PeriodicAction(
            Action.TypeOfAction.PUBLISH,
            CPUfanSpeed_fan2box,
            "tcp://localhost:35835",
            now,
            new TemporalPredicate(5)));
    followerDeterministicPorts.add(CPUTemperature_CPU2fanController);

    // The <key> is the id of the external port linked with a port of this model (<value>)
    portMap.put("CPUfanSpeed_fan2box", CPUfanSpeed_fan2box);
    portMap.put("CPUfanSpeed", CPUfanSpeed_fan2box);

    portMap.put("CPUTemperature_CPU2fanController", CPUTemperature_CPU2fanController);
    portMap.put("CPUTemperature", CPUTemperature_CPU2fanController);

    addNewInputPort(
        "CPUTemperature_CPU2fanController", "tcp://localhost:35983", "tcp://localhost:35169");

    model.set("targetTemperature", 65);
    model.set("Kp", 5.0);

    model.set("CPUTemperature", 25);
  }

  @Override
  public void onTime(Action currentAction, StopCondition sr) {
    if (now.compareTo(currentAction.temporalHorizon) == 0) {
      // Execute the corresponding action
      if (currentAction.port.ID.compareTo(CPUfanSpeed_fan2box.ID) == 0) {
        double value = (double) model.get(CPUfanSpeed_fan2box.associatedModelVariableName);

        publish(CPUfanSpeed_fan2box, value, now);
      }
      // Execute the corresponding action
      if (currentAction.port.ID.compareTo(CPUTemperature_CPU2fanController.ID) == 0) {
        double value = (double) currentAction.getValue();

        model.set("CPUTemperature", value);
        if (CONFIG.showDebugMessage)
          System.out.println(
              "["
                  + ID
                  + "] CPUTemperature_CPU2fanController = "
                  + model.get("CPUTemperature")
                  + " @ "
                  + now.doubleValue());
      }

      // Extra plot to show the actual data nature of the variable
      // Remove the current action from the to-do list
      currentAction.setDone();

      if (currentAction instanceof PeriodicAction) {
        todo.add(
            new PeriodicAction(
                currentAction.typeOfAction,
                currentAction.port,
                currentAction.hostSource,
                now,
                ((PeriodicAction) currentAction).getTemporalPredicate()));
      }
    }
  }

  @Override
  public void onEvent(Action currentAction, StopCondition sc) {}

  @Override
  public void onReadyToRead(Action currentAction, StopCondition sc) {}

  @Override
  public void onUpdated(Action currentAction, StopCondition sc) {
    // Not implemented yet
  }

  @Override
  public void onSync(Action currentAction) {
    // Statically generated ONLY if the BI abstracts a FMU
    // model.doStep(new TemporalPredicate(0));
    onTime(currentAction, null);
  }

  @Override
  public CoordinationPredicate setInitiatorsPredicate() {
    return null;
  }

  @Override
  public void onEnd() {
    stopwatch.stop(); // optional
    System.out.println("[fanController] Elapsed time: " + stopwatch.elapsed(TimeUnit.SECONDS));
  }
}
