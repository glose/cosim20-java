package CPUCoolingSystemWithFaultInjection;

import fr.inria.glose.cosim20.*;
import fr.inria.glose.cosim20.interfaces.*;
import org.eclipse.gemoc.execution.commons.commands.*;
import org.eclipse.gemoc.execution.commons.predicates.*;
import fr.cnrs.i3s.luchogie.oscilloscup.client.OscilloscupClient;
import java.io.IOException;
import fr.inria.glose.cosim20.CONFIG;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import com.google.common.base.Stopwatch;

public class CPUinBox extends CoordinationInterface {

  // -------------------------------
  // Plotter stuff
  // -------------------------------
  OscilloscupClient plotterClient;
  Stopwatch stopwatch = Stopwatch.createStarted();

  // ----------------------------------------------------------------
  // FMI Standard
  // ----------------------------------------------------------------
  double startTime = 0.0;
  double stopTime = CONFIG.EndOfSimulation.doubleValue();
  String fmuPath =
      "src/test/resources/CPUCoolingSystemWithFaultInjection/CPUinBoxWithFanHeatModel.fmu";

  public FollowerDeterministicPort CPUfanSpeed_fan2box =
      new FollowerDeterministicPort(
          "CPUfanSpeed_fan2box", "CPUfanSpeed", "fanController", 0, new TemporalPredicate(5));
  PrintWriter writer4cpufanspeed_fan2box;
  public Port CPUTemperature_CPU2fanController =
      new InitiatorPort(
          "CPUTemperature_CPU2fanController", "CPUTemperature", "fanController", 0.0); // OUTPUT
  PrintWriter writer4cputemperature_cpu2fancontroller;
  public Port CPUTemperature_box2ctrl =
      new FollowerPort("CPUTemperature_box2ctrl", "CPUTemperature", "overHeatController", 0.0);
  PrintWriter writer4cputemperature_box2ctrl;
  public Port isStopped_ctrl2box =
      new FollowerPort("isStopped_ctrl2box", "isStopped", "overHeatController", false);
  PrintWriter writer4isstopped_ctrl2box;
  public Port fanIsBroken_faultInjector =
      new FollowerPort("fanIsBroken", "fanIsBroken", "FaultInjector", 0);
  PrintWriter writer4fanIsBroken;

  public CPUinBox() {
    super("CPUinBox", "localhost", 35983, 35169, 3, 2);

    // Setup the FMI model
    model = new FMIInterface(fmuPath, startTime, stopTime);

    try {
      plotterClient = new OscilloscupClient("localhost", 40273);
    } catch (IOException e) {
      e.printStackTrace();
    }

    try {
      writer4cpufanspeed_fan2box = new PrintWriter(new File("CPUinBox_cpufanspeed_fan2box.csv"));
      writer4cpufanspeed_fan2box.write("time,CPUfanSpeed\n");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    try {
      writer4cputemperature_cpu2fancontroller =
          new PrintWriter(new File("CPUinBox_cputemperature_cpu2fancontroller.csv"));
      writer4cputemperature_cpu2fancontroller.write("time,CPUTemperature\n");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    try {
      writer4cputemperature_box2ctrl =
          new PrintWriter(new File("CPUinBox_cputemperature_box2ctrl.csv"));
      writer4cputemperature_box2ctrl.write("time,fanIsBroken\n");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    try {
      writer4isstopped_ctrl2box = new PrintWriter(new File("CPUinBox_isstopped_ctrl2box.csv"));
      writer4isstopped_ctrl2box.write("time,isStopped\n");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    try {
      writer4fanIsBroken = new PrintWriter(new File("CPUinBox_fanIsBroken.csv"));
      writer4fanIsBroken.write("time,fanIsBroken\n");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    initiatorPorts = new ArrayList<>();
    followerPorts = new ArrayList<>();
    followerDeterministicPorts = new ArrayList<>();

    followerDeterministicPorts.add(CPUfanSpeed_fan2box);
    initiatorPorts.add(CPUTemperature_CPU2fanController);
    todo.add(
        new PeriodicAction(
            Action.TypeOfAction.PUBLISH,
            CPUTemperature_CPU2fanController,
            "tcp://localhost:35983",
            now,
            new TemporalPredicate(5)));
    followerPorts.add(CPUTemperature_box2ctrl);
    followerPorts.add(isStopped_ctrl2box);
    followerPorts.add(fanIsBroken_faultInjector);

    // The <key> is the id of the external port linked with a port of this model (<value>)
    portMap.put("CPUfanSpeed_fan2box", CPUfanSpeed_fan2box);
    portMap.put("CPUfanSpeed", CPUfanSpeed_fan2box);

    portMap.put("CPUTemperature_CPU2fanController", CPUTemperature_CPU2fanController);
    portMap.put("CPUTemperature", CPUTemperature_CPU2fanController);

    portMap.put("CPUTemperature_box2ctrl", CPUTemperature_box2ctrl);
    portMap.put("CPUTemperature", CPUTemperature_box2ctrl);

    portMap.put("SwitchCPUState_ctrl2box", isStopped_ctrl2box);
    portMap.put("isStopped_ctrl2box", isStopped_ctrl2box);
    portMap.put("isStopped", isStopped_ctrl2box);

    portMap.put("fanIsBroken_faultInjector", fanIsBroken_faultInjector);
    portMap.put("fanIsBroken", fanIsBroken_faultInjector);

    addNewInputPort("CPUfanSpeed_fan2box", "tcp://localhost:35835", "tcp://localhost:43291");
    addNewInputPort("CPUTemperature_box2ctrl", "tcp://localhost:43587", "tcp://localhost:33395");
    addNewInputPort("SwitchCPUState_ctrl2box", "tcp://localhost:43587", "tcp://localhost:33395");
    addNewInputPort("fanIsBroken", "tcp://localhost:38399", "tcp://localhost:42611");

    model.set("CPUfanSpeed", 0);
    model.set("isStopped", false);
  }

  @Override
  public void onTime(Action currentAction, StopCondition sr) {
    if (now.compareTo(currentAction.temporalHorizon) == 0) {
      // Execute the corresponding action
      if (currentAction.port.ID.compareTo(CPUfanSpeed_fan2box.ID) == 0) {
        double value = (double) currentAction.getValue();

        model.set("CPUfanSpeed", value);
        if (CONFIG.showDebugMessage)
          System.out.println(
              "["
                  + ID
                  + "] CPUfanSpeed_fan2box = "
                  + model.get("CPUfanSpeed")
                  + " @ "
                  + now.doubleValue());
        try {
          plotterClient.addPoint(
              "cosimPlotter", 0, now.doubleValue(), (double) currentAction.getValue());
          writer4cpufanspeed_fan2box.write(
              now.doubleValue() + "," + currentAction.getValue() + "\n");
          writer4cpufanspeed_fan2box.flush();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      // Execute the corresponding action
      if (currentAction.port.ID.compareTo(CPUTemperature_CPU2fanController.ID) == 0) {
        double value =
            (double) model.get(CPUTemperature_CPU2fanController.associatedModelVariableName);

        publish(CPUTemperature_CPU2fanController, value, now);
        try {
          plotterClient.addPoint("cosimPlotter", 1, now.doubleValue(), value);
          writer4cputemperature_cpu2fancontroller.write(now.doubleValue() + "," + value + "\n");
          writer4cputemperature_cpu2fancontroller.flush();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      // Execute the corresponding action
      if (currentAction.port.ID.compareTo(CPUTemperature_box2ctrl.ID) == 0) {
        double value = (double) model.get(CPUTemperature_box2ctrl.associatedModelVariableName);

        publish(CPUTemperature_box2ctrl, value, now);

      }
      // Execute the corresponding action
      if (currentAction.port.ID.compareTo(isStopped_ctrl2box.ID) == 0) {
        boolean value = (boolean) currentAction.getValue();

        model.set("isStopped", value);
        if (CONFIG.showDebugMessage)
          System.out.println(
              "["
                  + ID
                  + "] isStopped_ctrl2box = "
                  + model.get("isStopped")
                  + " @ "
                  + now.doubleValue());
        try {
          plotterClient.addPoint(
              "cosimPlotter",
              3,
              now.doubleValue(),
              ((boolean) currentAction.getValue() == false) ? 0 : 20);
          writer4isstopped_ctrl2box.write(
              now.doubleValue() + "," + currentAction.getValue() + "\n");
          writer4isstopped_ctrl2box.flush();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      // Execute the corresponding action
      if (currentAction.port.ID.compareTo(fanIsBroken_faultInjector.ID) == 0) {
        boolean value = ((double) currentAction.getValue() > 0)?true:false;

        model.set("fanIsBroken", value);
        try {
          plotterClient.addPoint("cosimPlotter", 2, now.doubleValue(), (double) currentAction.getValue() * 20);
          writer4fanIsBroken.write(now.doubleValue() + "," + value + "\n");
          writer4fanIsBroken.flush();
        } catch (IOException e) {
          e.printStackTrace();
        }

        if (CONFIG.showDebugMessage)
          System.out.println(
              "["
                  + ID
                  + "] fanIsBroken_faultInjector = "
                  + model.get("fanIsBroken")
                  + " @ "
                  + now.doubleValue());
      }

      // Extra plot to show the actual data nature of the variable
      try {
        plotterClient.addPoint(
            "cosimPlotter",
            3,
            now.doubleValue(),
            (double) model.get(isStopped_ctrl2box.associatedModelVariableName) * 20);
      } catch (IOException e) {
        e.printStackTrace();
      }

      try {
        plotterClient.addPoint("cosimPlotter",
                2,
                now.doubleValue(),
                (double) model.get(fanIsBroken_faultInjector.associatedModelVariableName) * 20);
      } catch (IOException e) {
        e.printStackTrace();
      }

      try {
        plotterClient.addPoint("cosimPlotter",
                4,
                now.doubleValue(),
                (double) model.get("BoxTemperature"));
      } catch (IOException e) {
        e.printStackTrace();
      }

      // Remove the current action from the to-do list
      currentAction.setDone();

      if (currentAction instanceof PeriodicAction) {
        todo.add(
            new PeriodicAction(
                currentAction.typeOfAction,
                currentAction.port,
                currentAction.hostSource,
                now,
                ((PeriodicAction) currentAction).getTemporalPredicate()));
      }
    }
  }

  @Override
  public void onEvent(Action currentAction, StopCondition sc) {}

  @Override
  public void onReadyToRead(Action currentAction, StopCondition sc) {}

  @Override
  public void onUpdated(Action currentAction, StopCondition sc) {
    // Not implemented yet
  }

  @Override
  public void onSync(Action currentAction) {
    // Statically generated ONLY if the BI abstracts a FMU
    model.doStep(new TemporalPredicate(0));
    onTime(currentAction, null);
  }

  @Override
  public CoordinationPredicate setInitiatorsPredicate() {
    return null;
  }

  @Override
  public void onEnd() {
    writer4cpufanspeed_fan2box.flush();
    writer4cputemperature_cpu2fancontroller.flush();
    writer4cputemperature_box2ctrl.flush();
    writer4isstopped_ctrl2box.flush();
    writer4fanIsBroken.flush();
    stopwatch.stop(); // optional
    System.out.println("[CPUinBox] Elapsed time: " + stopwatch.elapsed(TimeUnit.SECONDS));
  }
}
