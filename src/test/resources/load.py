import sys
import numpy
import os
import subprocess
import time
from pyfmi import load_fmu
import pylab as P

PATH_FMU1=""
PATH_FMU2=""
NAME_FMU1="CPUinBoxWithFanHeatModel.fmu"
START_TIME=0
FINAL_TIME=10000
CPUtemp = []
boxTemp = []
time = []

#modelSem - TFSM
modelSem=load_fmu(NAME_FMU1,log_level=7) #upto 7
#modelElectrical - MODELICA
#modelElectrical.set("openclose",1)

#get the all Variables of the model
#variables = modelSem.get_model_variables()
#for v in variables.keys():
	#print v
currentTime=START_TIME
option=modelSem.simulate_options()
#no print simulation results
for i in range(START_TIME,FINAL_TIME, 10):
  #print("simulo")
  if(i > START_TIME):
      option['initialize']=False
  #time.sleep(0.5)
  #simulate the first model
  sys.stdout = open(os.devnull, "w")
  res=modelSem.simulate(options=option,start_time=currentTime, final_time=i+1)
  currentTime = res['time'][-1]
  if(i%2500 == 0):
    modelSem.set("CPUfanSpeed",10)
  else:
    modelSem.set("isStopped",1)
  if(currentTime%10000 <= 5000):
    modelSem.set("isStopped",0)
  else:
    modelSem.set("isStopped",1)
  #print("simulo2")
  CPUtemp.extend(res['CPUTemperature'])
  boxTemp.extend(res['BoxTemperature'])
  time.extend(res['time'])
  sys.stdout = sys.__stdout__
  #print (res["time"])



#print(CPUtemp)
fig = P.figure()
P.clf()
P.subplot(211)
line = P.plot(time, CPUtemp)
P.ylabel('CPU temperature')
P.xlabel('Time (s)')
P.setp(line,color='r')

#Resistor
P.subplot(212)
line = P.plot(time, boxTemp)
P.setp(line,color='g')
P.ylabel('box temperature')
P.xlabel('Time (s)')

#plot
P.show()
