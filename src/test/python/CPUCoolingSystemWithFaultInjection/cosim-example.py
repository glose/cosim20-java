import json
import linecache
import sys
import time
from threading import Thread

import zmq
from zmq.utils.strtypes import b


def sync(bind_to):
    # use bind socket + 1
    sync_with = ':'.join(bind_to.split(':')[:-1] +
                         [str(int(bind_to.split(':')[-1]))])
    ctx = zmq.Context.instance()
    s = ctx.socket(zmq.REP)
    print("Synchronization uri: ", sync_with)
    s.bind(sync_with)
    print("Waiting for subscriber to connect...")
    s.recv()
    print("   Done.")
    s.send_string("GO")


def queue():
    # start_input_queue()
    pass


def model():
    start_model()


def start_input_queue():
    print("Start input queue")
    # Socket to talk to server
    context = zmq.Context()
    socket = context.socket(zmq.SUB)

    # Subscribe to zipcode, default is NYC, 10001
    topicfilter = "outputs_value"
    socket.setsockopt_string(zmq.SUBSCRIBE, topicfilter)

    # Process 5 updates
    total_value = 0
    for update_nbr in range(5):
        string = socket.recv()
        topic, messagedata = string.split()
        total_value += int(messagedata)
        print(topic, messagedata)


def start_model():
    time.sleep(1)
    print("Open Actions file")
    line_counter = 1
    while line_counter <= len(open('actions.scenario').readlines()):
        line = linecache.getline('actions.scenario', line_counter)
        internal_time = line.rstrip().split(' ')[0].split('@')[1]
        value = line.rstrip().split(' ')[3]
        id = line.rstrip().split(' ')[2]
        topic = id
        messagedatarow = {
            "id": id,
            "temporalHorizon": float(internal_time),
            "value": int(value),
            "source" : "tcp://localhost:40751",
            "typeOfAction" : "SET",
        }
        messaged = json.dumps(messagedatarow)
        socket.send_multipart([b(topic), b(messaged)])
        print("Sent %s %s", topic, messaged)
        line_counter += 1

# Define output queues
port = "38399"
if len(sys.argv) > 1:
    port = sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

sync("tcp://*:42611")

print("Listen on port tcp://*:%s" % 38399)
# Define sync barrier

# Launch two threads: one handling the input queue
t1 = Thread(target=queue)
# one handling the model
t2 = Thread(target=model)

print("Start threads")
t1.start()
t2.start()

print("joint threads")
# Wait until the model completes its execution
t1.join()
t2.join()
